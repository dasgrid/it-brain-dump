Brain Dump Documentation
========================

<p>If you have call outs for inaccuracy or better methods, I'm open to all of them. I've been in IT for almost 10 years, I'm very thick skinned so don't sugar coat it! If you see a problem or would like to add in your documentation, feel free to fork and then place your PR request. I'm doing this to not only help myself but help others as well. Tips on how I document is more than welcome here too along with some secret sauce others may know when it comes to sphinx with [Read the Docs](https://readthedocs.org/)! Shout out to those who keep Read the Docs open and free!</p>

Here's the doucmentation on [Read the Docs](https://brain-dump.readthedocs.io/en/latest/)

Purpose

<p>This documentation is very much in active development and will be updated regularly. As of March, 2023, this is fairly empty but I'm still learning the flow here with rst files and how to build into html on the `Read the Docs` site. As much as I like markdown language for it's simplicity, reStructuredText is far more powerful when in use with sphinx.</p>

<p>Also I want to share what I know alo`1ng with providing myself a simple place to review on things forgotten or would rather not ever have to figure out again. I hope that someone 
finds my brain dump here helpful and save time. It's often that documentation fails me and I've lost count how many times I've lost days of progress because 1 or 2 important pieces of the documentation that was missing.</p>

Goals

* A place for supplemental documenation after reviewing the offical ones.
* Speed up review or helping someone more quickly jumpstart understanding of IT related technology.
* Tips and tricks I've learned and continue to use.
* Recommendations to configurations in a way that's simple to locate.
* Provide templates in code such as `Cloud-Init`, `Ansible` or even share parts of my `.bashrc` file.
* Provide actual full examples. Sometimes the `man` page or `api` documenation could be better. By the way, thank you `tldr`.

What This Documentation Is NOT...

* A rewritten tutorial or teaching how to use technology.
* A tutorial of commands and internal workings of the system but there are some exceptions to this.
* For Tech Support

Something Else

* Haven't figured out its place or format yet, but a place to where questions can be asked or just asking for help. Maybe even some project ideas.
* One thing I would like to finally get figured out, well when I have time to do actual digging, is create an entirely unattended install of NixOS on baremetal hardware.

**Warning**

<p>This documentation is ONLY a place to share what I've learned. It's up to the reader to do their own research and testing. It is also expected that any recommendations anywere here doesn't mean it would be suitable to your environment. Interpret this documentation and use at your own risk!</p>
