Linux Tips and Tricks
=======================

Alias bypass
---------------------

* Run a command in bash that is aliased but run it without the alias by prepending a backslash to the command.
* Useful if you're suspecting someone tappered with your .bashrc file
    * Ex. ``$ \ls``

Copy ssh-keys to remote server for passwordless authentication
---------------------------------------------------------------

* Utilize the builtin ssh key copy utility without having to ``scp`` it.
* If using asynchronious ssh keys (default), this will copy the public key over to your remote server.

..  code-block:: bash

    $ ssh-copy-id $USERNAME@$REMOTE_SERVER
    $ ssh-copy-id -i $PATH_TO_PUBLIC_KEY $USERNAME@$REMOTE_SERVER

.. note::
    The key you're copying over must be active in your ssh-agent.
    Also if your public ``$KEY_NAME`` doesn't end in ``.pub``, it will be appended automatically during the file transfer.

TLDR Pages and MAN
----------------------------------------------

* TLDR pages = tl;dr of man/info pages which shows examples of the commands and what they do.
* Install `tldr` package and build the local tldr documentation.
    * Ex. ``$ sudo {apt|apt-get|dnf|yum|whatever} install tldr``
    * Ex. ``$ tldr --update``
        * This will pull down on the TLDR pages using ``git`` which should be installed.
    * Ex. ``$ tldr find`` to find quick commands for find.

* MAN can be ran directly on configuration files to explain what means what.
    * Ex. ``$ man /etc/nsswitch.conf``
    * Note: Not all configuration files have this support but can help.

VI/VIM/NeoVIM
---------------------

* Have each line numbered
    * ``: set number``
* Shortest ways to save and quit
    * ``:x``
    * Press: ``SHIFT+ZZ`` (Z twice in a row)
* SED while the editor is open
    * ``:%s/oldthing/newthing/g``
* AWK while the editor is open
    * You will need to specify the line at first.
    * Ex. ``:$LINE_NUMBER,$!awk '{print}'``
* Copy a single line via the ``yank`` feature
    * Go to the line you want and press ``yy`` (The ``y`` key twice)
    * To paste, press 'p' or make it uppercase.
        * 'p' = past line below were curser is at.
        * 'P' = past line above were the curser is at.
* Run external command.
    * ``:! $COMMAND $COMMAND_ARGS``
    * Note: The output of the command will be added to the text file you're editing so be aware.
* View redundent or trailing white space with red highlighting.
    * Add and updated your ``~/.vimrc`` file for the following lines.
    * Never be fearful in adding options to your ``~/.vimrc`` file!

..  code-block:: bash

    $ vim ~/.vimrc
    #Add in these 2 lines
    highlight RedundantSpaces ctermbg=red guibg=red 
    match RedundantSpaces /\s\+$/


CTRL+S, A bad habit in a Linux Terminal
------------------------------------------

* If you accidentatlly pressed ``CTRL+S`` in your terminal, you will find that you have locked it up.
* Just press ``CTRL+Q`` to unfreeze the terminal

MULTITAIL Command
---------------------

* Instead of ``tail -f`` on multiple files, ``multitail`` will automatically multiplex your terminal for easier reading
    * Ex. ``sudo multitail $FILE $ANOTHERONE``

Record your command line session via ``script`` utility.
---------------------------------------------------------------

* Not sure why its called script but its very helpful for documentation.
* Will output all your commands and their output into a file.
    * Ex. ``$ script $LOG_FILE``

Adjusting Font and Fontsize while in a non-graphical mode on Linux (Specifically RHeL[89])
---------------------------------------------------------------------------------------------------------

* Handy for RDP connections to other nix based hosts which is useful for emulating a test simulation environment with limited access to anything but the objectives
    * Ex. The Redhat exams

* Font types and their size can be changed as long as the fonts and sizes exist. There doesn't seem to be a method of scaling out the font size here.
    * ``$ setfont $FONT_NAME``
        * Ex. ``$ setfont sun12x22``
        * Note the 12 and 22 here, these are valid font sizes for the fontname ``sun``.
        * Path to fonts: ``/usr/lib/kbd/consolefonts/``

* For configuration persistance
    * Global: /etc/vconsole.conf
    * By User: Just specifiy the commands in the dot file for the shell.
        * ``~/.bashrc``
        * Append ``setfont sun12x22``
        * If you want to swap between font 12 to 22, add a 2nd configuration line in the shell file.
            * Ex. ``setfont -22``

Extend Laptop Battery On Linux
----------------------------------

.. note::

    In my experience and while troubleshooting online, it's sad to know that 
    PC manufactures do not actually help much with power management when it 
    comes to Linux Laptops. Here is the best I have to increase your battery life.

.. code-block::

    Follow these commands in order!

    sudo {apt|dnf} install powertop2tuned tuned
    sudo powertop2tuned --enable powertop2tuned
    # Note here I'm not using the `--now` parameter.
    sudo systemctl enable tuned.service
    sudo systemctl reboot

    # After the reboot, if you want to see some validation it's working...
    # This will show you the mode you're in. There are several profiles you can pick :D
    sudo tuned-adm active

    # To enable dynamic tuning on the fly, access the `/etc/tuned/tuned-main.conf` file.
    # Search for `dynamic_tuning` and set it to `1` which a restart will be required.

    # Your system will now have power tuned with the tuned damon running.
    # I get an extra hour but at least I get more time.
    # Know better ways? Please see the home page of this documentation to let me know.

Tweak Bluetooth
--------------------

It doesn't matter what the OS is Linux, BSD, Windows or MacOS in my experience when it comes to the stability of bluetooth.

Here we will tune Bluetooth so it will reconnect faster and be more stable, well at least I hope this works for you!

.. code-block::

    # On Linux
    # Edit /etc/bluetooth/main.conf

    1. Uncomment `FastConnectable` and set it to `true`
    2. Uncomment `ReconnectAttempts` and set it to a number you wish. I chose `7`
    3. Uncomment `ReconnectIntervals` and set it to `1, 2, 3`

    sudo systemctl restart bluetooth
