Unattended Ubuntu Cloud Install
==========================================

This document will focus more on Cloud/VM installs but can be adapted to baremetal with additional code.
What an unattended does is self install and configure the host without any ad-hoc tasks. It's fully automated.

There are two tools to make this work, please be sure you have reviewed their offical documentation.

* `Cloud-Init Documentation <https://cloudinit.readthedocs.io/en/latest/>`_.
* `Ansible Documentation <https://docs.ansible.com/ansible/latest/index.html>`_.


Goals of this Unattended Install
----------------------------------

We are building a simple image here and if I had this document at the time of figuring this all out, I would have saved so much time!!!

The install will achieve the following:

1. Create a Ubuntu Cloud image
2. The image will have sshd configured to allow the ``nope`` user to login via a ssh key.
3. UFW will be enabled to allow ssh.
4. At first start up, pull down and run a simple ansible playbook to update the firewall and a few other things.


Environment Requirements
-------------------------

The host which you will be building these unattended images will need the following.

OS

* Any modern Linux based OS. Can be done on MacOS or Windows but the focus here is Linux.
* Ex. Ubuntu/Pop_OS! 22.04/Fedora 37

Packages

* ``$ sudo {apt|dnf} install virt-manager qemu-img ansible``

Hardware

* Virtualization enabled in BIOS.
* 1 free CPU core
* 2G free memory
* 10G minimum free disk space.

Setup Build Environment
------------------------

Cloud-Init Setup
^^^^^^^^^^^^^^^^^^

You will need to set up a directory you will use as your playground for building these unattended images. The code block below is how I have set up the directory.

.. code-block::

    mkdir ~/cloudinit-playground/
    cd ~/cloudinit-playground/
    touch user-data meta-data vendor-data
    wget https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img
    cp jammy-server-cloudimg-amd64.img ~/

   # user-data - This is your actual cloud-init configuration file.
   # meta-data - Just stores metadata about what the VM will. Can be used to configure a hostname.
   # vendor-data - Leave as an empty file!
   # A cloud based image suitable for the CPU architecture the ``.img`` can run on.
   # Ex. ``[Ubuntu Jammy](https://cloud-images.ubuntu.com/jammy/current/)``
   # See the webpage for ``.img`` files with the word ``cloudimg`` included.

.. note::

    Each time you build an images via cloud-init, the source ``.img`` file will be modified so back it up.
    In the above code block, you make a copy. Each time you rebuild, you will need to copy that original image to the ~/cloudinit-playground/

    Test your system with the [Cloud-Init Tutorial](https://cloudinit.readthedocs.io/en/latest/tutorial/qemu.html)

Ansible Setup
^^^^^^^^^^^^^^

If not done already, setup your own location to setup and store your playbooks and inventory file. The code block below is just for someone who wants to learn.

.. code-block::

    mkdir ~/ansible-playground/
    cd ~/ansible-playground/
    touch inventory.ini playbook.yml
    # Using ansible-galaxy, which is included when you install ansible, install community.general. These are so handy! :D
    sudo ansible-galaxy collection install community.general
    

General Cloud-Init Workflow
------------------------------------

1. See ``Cloud-Init Example`` section for example files for fun on the left pane of this document.
2. Be sure you are in the working directory of your ``~/cloudinit-playground/``.
3. Update your ``user-data`` file.
4. Launch a simple HTTP server via python module in the background. Be sure the path goes to where the img and data files are located.
    * Ex. ``$ python3 -m http.server --directory ~/cloudinit-playground/ &``
    * This will be used as the supplier of the ``user-data``, ``meta-data`` and ``vendor-data`` files.

5. Be sure that ``vendor-data`` file is empty.
6. See if it builds running the following code block below. This will launch the vm in the terminal where its pasted were you can see all console output.

..  code-block:: bash

    qemu-system-x86_64                                            \
      -net nic                                                    \
      -net user                                                   \
      -machine accel=kvm:tcg                                      \
      -cpu host                                                   \
      -m 512                                                      \
      -nographic                                                  \
      -hda jammy-server-cloudimg-amd64.img                        \
      -smbios type=1,serial=ds='nocloud-net;s=http://10.0.2.2:8000/'

.. note::
    The above code block was taken directly from the offical documentation in Cloud-Init

5. Once the image is done being built, press `ctrl a` and then `e` to exit out of the qemu session. Now you have your generated image.
6. Test the image in virt-manager, virtualbox or whatever other virtualization tool you're using. If it works as intended, you got a simple but good image.

General Ansible Workflow
----------------------------

0. See ``Ansible Example`` section for example files for fun on the left pane of this document.
1. Once you have your inventory file and playbook set up, time to test.

.. code-block:: 

    ansible-playbook /path/to/playbook.yml --syntax-check
    ansible-playbook /path/to/playbook.yml --check # This is equivalant to a dry run
    ansible-playbook /path/to/playbook.yml -i /path/to/inventory.ini --limit 'lab' # If you chose to call a group in a playbook
    ansible-playbook /path/to/playbook.yml --limit "hostname" # For just a single box.

2. Ansible will report back anything that had changed.

Some Gotchyas Here - README!!!
------------------------------------

.. note:: 
    When building an image, the amount of disk space you have is very small and when adding in ansible components on your build will very likely take up more space than what is available.
    To get around this I recommend the following:

        * Run ``$ qemu-img resize /PATH/TO/IMAGE/Ubuntu.img +1G`` will add 1 Gb of disk space.
        * In your ``user-data`` file be sure the ``packages:`` block has ``cloud-initramfs-growroot`` listed so it gets installed.
        * This will only happen on the first boot where the root parition will grow but the build isn't considered first boot.
        * Be sure the following are in your ``user-data`` file.

    packages:
        - cloud-initramfs-growroot

    runcmd:
        - sudo growpart /dev/vda 1
        - sudo resize2fs /dev/vda1
        - Or for BTRFS setups
        - sudo growpart /dev/vda 1
        - sudo btrfs filesystem resize max /


.. note:: 
    Regarding ansible-pull or whatever other mechanisms are being used to automate ansible playbooks, update the apt database and upgrade it in the ``user-data`` file.
    Otherwise when downloading the ansible galaxy community packages, you will get a very dated package.
    If you haven't done this and are stuck on some silly syntax error, try rebuilding the image with the the options in the code block below.

..  code-block::

    # No spaces are needed for these on the file.
    # They are their own self contained actions.
    # Place this near the top of the user-data file.

    package_update: true
    package_upgrade: true


.. note:: 
    Reguarding the colon character in URLs, these are very problematic in yaml files and with cloud-init as well.
    If there is a task needed to be done with a URL then it's just better off to write a systemd service file calling a script.
    Then have under ``runcmd:`` in the user-data file, near the bottom, to have the service file restarted. Here's an example.

..  code-block::

    write_files:
    - path: /home/USER/bootstrap-yaml.sh
        permissions: '0777' # FIXME permissions, should just set the execute bit
        content: |
            #!/usr/bin/bash
            # Don't remove the growpart and resize2fs lines otherwise there won't be enough disk space for the host to download required packages on first boot.
            sudo growpart /dev/sda 1
            sudo resize2fs /dev/sda1
            sudo curl --header "PRIVATE-TOKEN: bogus-32890hbc9" "https://gitlab.com/api/v4/projects/00000000/repository/files/ansible%2Fplaybooks%2Flab%2Flab.yml/raw?ref=main" >> /home/USER/lab.yml
            sudo curl --header "PRIVATE-TOKEN: bogus-32890hbc9" "https://gitlab.com/api/v4/projects/00000000/repository/files/ansible%2Fplaybooks%2Finventory.ini/raw?ref=main" >> /home/USER/inventory.ini
            sudo apt install ansible -y
            sudo ansible-galaxy collection install community.general
            sudo ansible-playbook /home/USER/lab.yml
            sudo rm /home/USER/lab.yml /home/USER/inventory.ini /home/USER/bootstrap-yaml.sh
    - path: /etc/systemd/system/ansible-boot.service
        permissions: '0777' # FIXME permissions, should just set the execute bit
        content: | 
            [Unit]
            Description=Bootstraps ansible due to low initial drive space on cloud images
            [Service]
            User=root
            ExecStart=/usr/bin/bash -c /home/USER/bootstrap-yaml.sh
            [Install]
            WantedBy=default.target
    runcmd:
    - sudo chmod +x /home/USER/bootstrap-yaml.sh
    - sudo systemctl daemon-reload # Had the host yell at me about the systemd daemon needing reloading after saving the .service file spawned by the root user.
    - sudo systemctl restart ansible-boot.service


.. note:: 
    The default yaml file for netplan, that is if you are using a cloud image with it, will write a base file in /etc/netplan/ that could cause problems.
    I would remove it.

..  code-block:: bash

    runcmd:
    # For me this was the file needed to be removed on Ubuntu 22 Server
        - rm /etc/netplan/50-cloud-init.yaml

Cloud-Init Example
------------------------------------

For user-data
^^^^^^^^^^^^^^

.. note::
    Beyond a user setup and getting ssh going I've had to build out another way to perform an ansible pull.
    Since the ``user-data`` file is yaml, the ``:`` character causes problems and to pull from a private repo with the Gitlab API, a ``:`` is required.
    Basically I build a systemd service file to call a bash script to pull down what I need for ansible to run and then work it in the order of operations of Cloud-Init.

    If you have a better way, I'm open to hearing about it!

    * In my mind, I would rather not pull down an entire repo on a server just to run an ansible playbook.
    * I don't host my own gitlab so I need to pull from private that hasn't authenticated with Gitlab before.
    * I could make a dumb user on gitlab with a key and add it in though. Hmmm... I'm still thinking about it.
    * I would rather not include the playbook in the actual image either because lets say your enironment changed or other services changed?

.. code::

    #cloud-config

    groups:
    - admingroup: [root,sys]
    - cloud-users

    # Required so apt installs the correct packages for ansible-galaxy and prevents old community.general.* collections version downloaded by ansible galaxy can actually run.
    package_update: true
    package_upgrade: true

    # Package cloud-initramfs-growroot is required to expand the disk while in the cloud. Added here to be sure I don't forget when moving to prod.
    packages:
    - git
    - cloud-initramfs-growroot

    users:
    - name: nope
        groups: sudo
        shell: /usr/bin/bash
        lock_passwd: false
        sudo: ALL=(ALL) NOPASSWD:ALL
        # LAB: Password is test, all lowercase.
        passwd: $6$2.Xjt/LeAAKczJWo$bjq4hq.7krwyJ9QwtOB..e1mFy8ObKG/W.OUOcLJ7xT63JXYMq2X3GvpvHTiN0YWc6zPEXFAD9kraozul0NeH1
        ssh_authorized_keys:
        # LAB: Passphrase is test, all lowercase.
        - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHL1PlQVhzvpGWoQJzUzvsi4dLXuu1HB4cvJuGAISguI LAB-TESTING

    # Both options are needed to not print keys/finger prints to console were it is also logged in /var/log/cloud-init*
    no_ssh_fingerprints: true
    ssh:
    emit_keys_to_console: false

    # Disable cloud-init from configuring the network and setup the custom netplan.yaml file. 
    write_files:
    - path: /etc/cloud/cloud.cfg.d/99-custom-networking.cfg
        permissions: '0644'
        content: |
        network: {config: disabled}
    - path: /etc/netplan/netplan.yaml
        permissions: '0644'
        content: |
        network:
            version: 2
            ethernets:
            eth0:
                match:
                name: en*
                dhcp4: true  
    - path: /home/nope/bootstrap-yaml.sh
        permissions: '0777' # FIXME permissions, should just set the execute bit
        content: |
            #!/usr/bin/bash
            # Don't remove the growpart and resize2fs lines otherwise there won't be enough disk space for the host to download required packages on first boot.
            sudo growpart /dev/sda 1
            sudo resize2fs /dev/sda1
            sudo curl --header "PRIVATE-TOKEN: bogus-32890hbc9" "https://gitlab.com/api/v4/projects/0000000/repository/files/ansible%2Fplaybooks%2Flab%2Flab.yml/raw?ref=main" >> /home/nope/lab.yml
            sudo curl --header "PRIVATE-TOKEN: bogus-32890hbc9" "https://gitlab.com/api/v4/projects/0000000/repository/files/ansible%2Fplaybooks%2Finventory.ini/raw?ref=main" >> /home/nope/inventory.ini
            sudo apt install ansible -y
            sudo ansible-galaxy collection install community.general
            sudo ansible-playbook /home/nope/lab.yml
            sudo rm /home/nope/lab.yml /home/nope/inventory.ini /home/nope/bootstrap-yaml.sh
    - path: /etc/systemd/system/ansible-boot.service
        permissions: '0777' # FIXME permissions, should just set the execute bit
        content: | 
            [Unit]
            Description=Bootstraps ansible due to low initial drive space on cloud images
            [Service]
            User=root
            ExecStart=/usr/bin/bash -c /home/nope/bootstrap-yaml.sh
            [Install]
            WantedBy=default.target

    # Removal of /etc/netplan/50-cloud-init.yaml needs to be removed before running the netplan commands.
    runcmd:
    - sudo chmod +x /home/nope/bootstrap-yaml.sh
    - sudo systemctl daemon-reload
    - sudo systemctl restart ansible-boot.service
    - rm /etc/netplan/50-cloud-init.yaml
    - sudo netplan generate
    - sudo netplan apply
    - sed -i 's/\#\?PubkeyAuthentication .\+/PubkeyAuthentication yes/' /etc/ssh/sshd_config
    - sed -i 's/\#\?PasswordAuthentication .\+/PasswordAuthentication no/' /etc/ssh/sshd_config
    - echo "AllowUsers nope" >> /etc/ssh/sshd_config 
    - echo "PermitRootLogin no" >> /etc/ssh/sshd_config
    - sudo systemctl restart sshd.service
    - sudo ufw enable

For meta-data
^^^^^^^^^^^^^^^^^^

.. code::

    instance-id: cloud-vm-lab
    local-hostname: cloud-vm-lab

Ansible Example
-----------------

For inventory.ini
^^^^^^^^^^^^^^^^^^

.. code-block::

    [prod]
    example.org

    [lab]
    cloud-vm-lab


Simple Ansible playbook
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

    ---
    - name: Update Lab Firewall
    hosts: localhost
    remote_user: nope
    become: true

    tasks:
    - name: Ensure UFW is enabled.
        community.general.ufw:
        state: enabled
        logging: on

    - name: SSH Allow List
        community.general.ufw:
        rule: allow
        port: '22'
        src: '{{ item }}'
        loop:
        - 192.168.1.0/24
        - 192.168.122.0/24
        - 192.168.123.0/24

    - name: Deny IPv6 SSH
        community.general.ufw:
        rule: deny
        protocol: tcp
        port: '22'
        src: "::"

    - name: Reload UFW
        community.general.ufw:
        state: reloaded

    - name: editor is vim
        ansible.builtin.shell:
        cmd: sudo update-alternatives --set editor /usr/bin/vim.basic

    - name: sudo-group-nopass
        community.general.sudoers:
        name: No Pass for SUDO Group
        group: sudo
        nopassword: true
        commands: ALL
    ...

