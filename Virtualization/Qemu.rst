Qemu and Virt-Manager
======================
.. note::
    Each time a VM XML file gets updated, It might be a good idea to reload the libvirt service. This has helped troubleshooting sound issues on Windows VMs.

Loop Mounts
------------

.. code-block::

    mount /path/ /mnt/
    mount -o loop,offset=(number here) /path/to/disk/image /where/to/mount
    # The number is 512 multipliedby the number under the start sector when you run fdisk -l against the disk image file 1048576

Creating virtual disks
------------------------

.. code-block::

    kvm-img create -f raw /srv/kvm/images/<servername>-vdb.img 10G

Expand VM Image with Contiguous Paritions - Linux
-----------------------------------------------------------

.. code-block::

    # Be sure the VM is off and then locate the virtual disk file.
    sudo virsh domblklist $VM_NAME
    
    # Resize VM
    sudo qemu-img resize $PATH_TO_VM_IMAGE +10G

    Or...

    # Note that the command below here, 40G will update the VM to a 40G disk size and not add 40G on top of what has already been allocated.
    sudo virsh blockresize $VM_NAME $PATH_TO_VM_IMAGE 40G

    # Start the VM and then run the following command
    sudo virsh blockresize $VM_NAME $PATH_TO_VM_IMAGE 60G
        # The 60G here will be the total. So of the VM had 40G before, and you added 20, you would state 60G.
        # On a Linux based VM, check dmesg and you will see that the size of the virtual disk has changed but only after booting it back up.
    
    # At this point start the VM and using the OS's own utilities, expand its partion to take full use of the expanded disk space.
    sudo growpart /dev/vda $PARTITION_NUMBER

    # Then run one of the following
        # BTRFS -> $ sudo btrfs filesystem resize max /
        # EXT4 -> $ sudo resize2fs /dev/$PARTITION
    # Note: You may run into issues here when you install OS's such as PopOS were there is a recovery partition.
    # The issue is due to discontigous paritions and when you expand a disk you will likely get an error that it cannot be extended. This is covered on the next section.

Expand VM Image with NON-Contiguous Paritions - Linux
----------------------------------------------------------------

When you have a file system you need expanded with non-contiguous, or where free disk space exists on the other end of another partition, this will cause some headaqes depending on your setup.

Inb4: Why not just do this in gui?

Omg you have no idea how annoying this is and all these errors kept coming up regardless if it was the gui form of ``gnome-disks``, ``gdisks`` or other applications I've tried. When you have ZRAM set up, the steps required do not appear supported with these gui utilities.

.. image:: media/non-contiguous-partitions.png
   :width: 700

Other things that need to be taken into account, here are some examples which depending one which is relevant to the reader which may need extra steps. I won't go over those but I do have a way to work around MOST of these issues, as in there will be a few which you will need to read into.

* ZRAM with Crypt (Ex. On Pop_OS), extra steps are needed.
* Luks encryption with Logical Volume management (LVM).
* Are you not using encryption?
* Using BTRFS?
* RAID? Are you also using RAID features in LVM?

At first this is going to look very similar to the above section. 
Be sure the VM is off and then locate the virtual disk file. 
To illustrate, we are going to resize the virtual disk for ``PopOS-22-Init-clone`` and will locate the virtual disk we will expand which for this vm is in ``/var/lib/libvirt/images/PopOS-22-Init-clone.qcow2``.

The current disk size is 25G and we are going to increase the disk by 10G.

.. code-block::

    $ sudo virsh list --all
    Id   Name                  State
    --------------------------------------
    -    PopOS-22-Init         shut off
    -    PopOS-22-Init-clone   shut off

    $ sudo virsh domblklist PopOS-22-Init-clone
    Target   Source
    -------------------------------------------------------------
    vda      /var/lib/libvirt/images/PopOS-22-Init-clone.qcow2
    sda      -

Now lets increase the disk for the VM. Again, be sure your VM is off!

.. code-block::

    $ sudo qemu-img resize /var/lib/libvirt/images/PopOS-22-Init-clone.qcow2
    
    OR

    $ sudo virsh blockresize PopOS-22-Init-clone /var/lib/libvirt/images/PopOS-22-Init-clone.qcow2 35G

    Example
    $ sudo qemu-img resize /var/lib/libvirt/images/POPOS-TEST-LAB-clone.qcow2 +10G
    Image resized.

Now lets turn back on the VM and login. If you have a desktop environtment, which isn't required here, you can open something like gnome disks or whatever disks utility you have to see how much free space you have now to allocate.

This is just for illustrative purposes.

.. image:: media/qemu-gnomedisks.png
   :width: 700


Continuing on, we will stop swap from running but we need to know how its set up. As you can see here we are using zram with encrypted swap.

Note the LABEL field on the 2nd command is ``cryptswap``, yours will likely be different but it's important to know the name here.

.. code-block::

    $ sudo swapon -s
    Filename				Type		Size		Used		Priority
    /dev/dm-0                               partition	4192764		0		-2
    /dev/zram0                              partition	4000764		0		1000

    $ lsblk -fs
    NAME      FSTYPE FSVER LABEL     UUID                                 FSAVAIL FSUSE% MOUNTPOINTS
    sr0                                                                                  
    zram0                                                                                [SWAP]
    vda1      ext4   1.0             0da77da9-1864-42ad-b121-845778625655   10.9G    42% /
    └─vda                                                                                
    cryptswap swap   1     cryptswap fedddcf5-4159-4e91-8d22-16fa0a858789                [SWAP]
    └─vda2    swap   1     cryptswap 4b7f592a-946b-46e8-9d9d-259d7b0df19c                
    └─vda   

Let's stop swap and since we have encrypted swap labeled as ``cryptswap``, which is an argument to a command we need to use.

.. code-block::

    This doesn't generate output.
    $ sudo swapoff -a
    
    $ sudo cryptdisks_stop cryptswap
     * Stopping crypto disk...                                                                                                             
     * cryptswap (stopping)...                             [ OK ] 

Verify that swap has stopped. In this output, allocated swap is 0B accoss the entry entry, this is what we want.

.. code-block::

    $ free -h
               total        used        free      shared  buff/cache   available
    Mem:       3.8Gi       1.1Gi       1.7Gi        20Mi       1.0Gi       2.4Gi
    Swap:         0B          0B          0B

Now we use fdisk, which should be already installed. If there was ever a Linux distro that didn't have this by default, I would be pretty surprised.

There are a few things we will need to keep in mind here, especually for those who don't use fdisk often or have never used it.

1. What virtual disk and parition on that disk is the swap associated with. Here the disk is just /dev/vda.
2. We will be deleting the current swap partition, in this example it is /dev/vda2 or the 2nd partition. You can see this running ``lsblk -fs``.
3. We will need to do some math to determine where to place the swap parition, that is if you still really want to have a swap parition and not a swap file. You will need to know the sector size and how large you want the swap space to be. 

Once we know were swap is, we can use the ``fdisk`` utility to examine it further, delete the current swap partition, resize the ``/`` directory and build another cryptswap.

.. caution::
    If you don't select the actual existing swap parition for deletion here, you will lose your VM! When runing fdisk, you can always quit via ctrl-c or pressing q and enter.

.. code-block::

    $ lsblk -fs
    NAME  FSTYPE FSVER LABEL     UUID                                 FSAVAIL FSUSE% MOUNTPOINTS
    sr0                                                                              
    zram0                                                                            
    vda1  ext4   1.0             0da77da9-1864-42ad-b121-845778625655   10.9G    42% /
    └─vda                                                                            
    vda2  swap   1     cryptswap 4b7f592a-946b-46e8-9d9d-259d7b0df19c <-- HERE! Swap is vda2.
    └─vda         0B          0B          0B

    $ sudo fdisk /dev/vda

    Welcome to fdisk (util-linux 2.37.2).
    Changes will remain in memory only, until you decide to write them.
    Be careful before using the write command.

    This disk is currently in use - repartitioning is probably a bad idea.
    It's recommended to umount all file systems, and swapoff all swap
    partitions on this disk.


    Command (m for help): p <-- This just lists out the current partitions.

    Disk /dev/vda: 35 GiB, 37580963840 bytes, 73400320 sectors
    Units: sectors of 1 * 512 = 512 bytes
    Sector size (logical/physical): 512 bytes / 512 bytes
    I/O size (minimum/optimal): 512 bytes / 512 bytes <-- Here is our sector size! Keep a note!
    Disklabel type: dos
    Disk identifier: 0xb14ce4d5

    Device     Boot    Start      End  Sectors Size Id Type
    /dev/vda1           4096 44036094 44031999  21G 83 Linux
    /dev/vda2       44042239 52428799  8386561   4G 82 Linux swap / Solaris

    Command (m for help): d
    Partition number (1,2, default 2): 2

    Partition 2 has been deleted. <-- Note that this wasn't actually deleted yet!

    Command (m for help): n # Create new parition
    Partition type
    p   primary (1 primary, 0 extended, 3 free)
    e   extended (container for logical partitions)
    Select (default p): p # Make it a primary parition
    Partition number (2-4, default 2): 2 # selected the next parition number. 
    First sector (2048-73400319, default 2048):
    
    Okay now lets explain this part bit in the next section.

This is where we need to do some math and we need to tell fdisk were to start the next parition for swap.

Fdisk has provided us a range, ``2048-73400319``. To translate, this means fdisk is asking, starting from 2048 sectors after the first parition, at what sector do we start?

73400319 is the last sector on the disk.

Lets say we want a 4G swap partition and you want it at the end of the disk. Here is what the logic and math looks like:

1. We want a 4G parition for swap, convert to Bytes, which is ``4293918720``. I honestly would just search online for something like "convert 4g to bytes" online.
2. Now we know that ``4293918720`` is in Bytes, is 4G. We need to know how many 512 byte sectors that is. So ``4293918720 / 512 = 8386560``
3. It will take ``8386560`` 512 Byte sectors to equal 4G.
4. We were informed by fdisk the final sector is ``73400319``. So lets subtract from that last sector, the number of sectors we need. ``73400319 - 8386560 = 65013759``.
5. So ``65013759`` is the sector we will start with.


.. code-block::

    Continuing on in fdisk

    Command (m for help): n
    Partition type
    p   primary (1 primary, 0 extended, 3 free)
    e   extended (container for logical partitions)
    Select (default p): p
    Partition number (2-4, default 2): 2
    First sector (2048-73400319, default 2048): 65013759
    Last sector, +/-sectors or +/-size{K,M,G,T,P} (65013759-73400319, default 73400319): < Press Enter >

    Created a new partition 2 of type 'Linux' and of size 4 GiB.

    We will need to update the partition type from `Linux` to `Linux swap`.

    In the fdisk menu, when you press 'm', you will notice that 't' needed.

    Command (m for help): t
    Partition number (1,2, default 2): 2
    Hex code or alias (type L to list all): L

    00 Empty            24 NEC DOS          81 Minix / old Lin  bf Solaris        
    01 FAT12            27 Hidden NTFS Win  82 Linux swap / So  c1 DRDOS/sec (FAT-
    02 XENIX root       39 Plan 9           83 Linux            c4 DRDOS/sec (FAT-
    03 XENIX usr        3c PartitionMagic   84 OS/2 hidden or   c6 DRDOS/sec (FAT-
    04 FAT16 <32M       40 Venix 80286      85 Linux extended   c7 Syrinx         
    05 Extended         41 PPC PReP Boot    86 NTFS volume set  da Non-FS data    
    06 FAT16            42 SFS              87 NTFS volume set  db CP/M / CTOS / .
    07 HPFS/NTFS/exFAT  4d QNX4.x           88 Linux plaintext  de Dell Utility   
    08 AIX              4e QNX4.x 2nd part  8e Linux LVM        df BootIt         
    09 AIX bootable     4f QNX4.x 3rd part  93 Amoeba           e1 DOS access     
    0a OS/2 Boot Manag  50 OnTrack DM       94 Amoeba BBT       e3 DOS R/O        
    0b W95 FAT32        51 OnTrack DM6 Aux  9f BSD/OS           e4 SpeedStor      
    0c W95 FAT32 (LBA)  52 CP/M             a0 IBM Thinkpad hi  ea Linux extended 
    0e W95 FAT16 (LBA)  53 OnTrack DM6 Aux  a5 FreeBSD          eb BeOS fs        
    0f W95 Ext'd (LBA)  54 OnTrackDM6       a6 OpenBSD          ee GPT            
    10 OPUS             55 EZ-Drive         a7 NeXTSTEP         ef EFI (FAT-12/16/
    11 Hidden FAT12     56 Golden Bow       a8 Darwin UFS       f0 Linux/PA-RISC b
    12 Compaq diagnost  5c Priam Edisk      a9 NetBSD           f1 SpeedStor      
    14 Hidden FAT16 <3  61 SpeedStor        ab Darwin boot      f4 SpeedStor      
    16 Hidden FAT16     63 GNU HURD or Sys  af HFS / HFS+       f2 DOS secondary  
    17 Hidden HPFS/NTF  64 Novell Netware   b7 BSDI fs          fb VMware VMFS    
    18 AST SmartSleep   65 Novell Netware   b8 BSDI swap        fc VMware VMKCORE 
    1b Hidden W95 FAT3  70 DiskSecure Mult  bb Boot Wizard hid  fd Linux raid auto
    1c Hidden W95 FAT3  75 PC/IX            bc Acronis FAT32 L  fe LANstep        
    1e Hidden W95 FAT1  80 Old Minix        be Solaris boot     ff BBT            

    Aliases:
    linux          - 83
    swap           - 82
    extended       - 05
    uefi           - EF
    raid           - FD
    lvm            - 8E
    linuxex        - 85
    Hex code or alias (type L to list all): 82 <-- What we want!

    Changed type of partition 'Linux' to 'Linux swap / Solaris'.

    Command (m for help): w <-- This is where we actually write changes to disk.
    The partition table has been altered.
    Syncing disks.

For illustrative purposes, here is what the layout should look like now. As you can see we are now have contiguous paritions.

.. image:: media/qemu-update-table.png
   :width: 700

Lets look at ``lsblk -fs`` once more. We now see the parition is there but swap isn't actually built out yet. \

.. code-block::

    $ lsblk -fs
    NAME  FSTYPE FSVER LABEL UUID                                 FSAVAIL FSUSE% MOUNTPOINTS
    sr0                                                                          
    zram0                                                                        
    vda1  ext4   1.0         0da77da9-1864-42ad-b121-845778625655   10.9G    42% /
    └─vda                                                                        
    vda2                                                                         
    └─vda  

Since we were using cryptswap with zram on this setup, extra steps will be needed.

.. code-block::

    $ sudo mkswap -L cryptswap /dev/vda2
    Setting up swapspace version 1, size = 4 GiB (4293914624 bytes)
    LABEL=cryptswap, UUID=ef33e2ca-498c-4965-b4a3-5959a5980ca6 <-- Grab this UUID

    Update /etc/cryptab with the new UUID.
    $ sudo vi /etc/crypttab

    Once done, it should look like this:
    $ cat /etc/crypttab 
    cryptswap UUID=ef33e2ca-498c-4965-b4a3-5959a5980ca6 /dev/urandom swap,plain,offset=1024,cipher=aes-xts-plain64,size=51

    Notice the name 'cryptswap' here!
    It's the first field and is the label we gave to our swap when we created it via mkswap.

Lets initialize cryptdisks for swap. You will need to use ``cryptswap`` name as an argument here.

.. code-block::

    $ sudo cryptdisks_start cryptswap
    * Starting crypto disk...
    * cryptswap (starting)...
    * cryptswap (started)...
  
    $ lsblk -fs
    NAME      FSTYPE FSVER LABEL     UUID                                 FSAVAIL FSUSE% MOUNTPOINTS
    sr0                                                                                  
    zram0                                                                                
    vda1      ext4   1.0             0da77da9-1864-42ad-b121-845778625655   10.9G    42% /
    └─vda                                                                                
    cryptswap swap   1               9f6a9162-fe03-41b4-8d4a-b82e7cab989f                
    └─vda2    swap   1     cryptswap ef33e2ca-498c-4965-b4a3-5959a5980ca6                
    └─vda

    $ ls -l /dev/mapper/
    total 0
    crw------- 1 root root 10, 236 Mar 27 19:23 control
    lrwxrwxrwx 1 root root       7 Mar 27 20:28 cryptswap -> ../dm-0
 
 Lets now enable swap.

 .. code-block::

    $ sudo swapon -a

    To verify run with -s for a summary of current swap devices.

We will need a utility called ``growpart`` to expand the parition of /dev/vda1. It's likely you will need to install this utility.

.. code-block::

    If you need to install growpart.

    APT: $ sudo apt install cloud-utils
    DNF: $ sudo dnf install cloud-utils-growpart

    Grow the parition

    $ sudo growpart /dev/vda 1 
    Yes there is a space between vda and the number 1

    Example

    $ sudo growpart /dev/vda 1
    CHANGED: partition=1 start=4096 old: size=44031999 end=44036095 new: size=65009663 end=65013759

Let's finally use that new disk space! Keep in mind what file system you're using and if you are using Logical Volume Management, for LVM for short.

.. code-block::

    Using EXT4 in my example.

    $ df -T
    Filesystem     Type  1K-blocks    Used Available Use% Mounted on
    tmpfs          tmpfs    400080    1500    398580   1% /run
    /dev/vda1      ext4   21494284 8977232  11399872  45% /
    tmpfs          tmpfs   2000396       0   2000396   0% /dev/shm
    tmpfs          tmpfs      5120       0      5120   0% /run/lock
    tmpfs          tmpfs    400076     140    399936   1% /run/user/1000
    tmpfs          tmpfs   2000396       0   2000396   0% /run/qemu

    EXT4: $ sudo resize2fs /dev/vda1
    BTRFS: $ sudo btrfs filesystem resize max /

    $ sudo resize2fs /dev/vda1
    resize2fs 1.46.5 (30-Dec-2021)
    Filesystem at /dev/vda1 is mounted on /; on-line resizing required
    old_desc_blocks = 3, new_desc_blocks = 4
    The filesystem on /dev/vda1 is now 8126207 (4k) blocks long.

    If using LVM, there more steps and we need to know some labels here.
    
    I highly recommend you read up on LVM if you haven't.

    We need to know the label setup by LUKS for your VM.

    In this example, it is "cryptdata" and is mapped to vda1.

    $ sudo dmsetup info -c -o name,blkdevname,devnos_used,blkdevs_used
    Name             BlkDevName       DevNosUsed       BlkDevNamesUsed 
    cryptdata        dm-0             259:3            vda1       
    cryptswap        dm-2             259:4            vda2       
    data-root        dm-1             253:0            dm-0     

    The "cryptdata" is your encrypted disk and is the default name, not to be confused with "data-root". We will need to extend the physical size of the disk.

    $ sudo pvresize /dev/mapper/cryptdata

    Now we update the logical extents to the maximum disk space we can resize.

    $ sudo lvresize -l +100%FREE /dev/mapper/data-root

We are almost done, just need to inform the host kernel of the changes, rebuild initramfs and reboot.

.. code-block::

    It will just run without output.
    $ sudo partprobe

    This could take a few minutes or more depending how many kernels you have installed.
    $ sudo update-initramfs -u -k all

    If you have grub installed. Don't worrie about this if you have full disks LUKS encryption setup.
    $ sudo update-grub2 

Reboot and enjoy!

Windows VMs (10/11) Require Tuning
-------------------------------------

* Before you install a new VM just be sure the following is done as this will save some headaqes down the road.
    * Install Packages For Linux/Debian: ``swtpm-tools`` and ``ovmf``
    * When you're creating a new VM, there is a checkbox to let you configure advanced settings before initial install.
        * From here you can configure everything else mentioned below all at once.
        * Recommendations for Windows 10/11 is 80G disk, 8G Ram and 8 CPU Threads. See ``CPU Settings`` section of this document.

* Regarding Upgrading from Windows 10 to 11
    * Use the health tool to check everything.

.. note::

    Even if the tool sees everything passing but the Windows Updater sees otherwise, download the Win11 install assistant anyway and the upgrade will occur

CPU Settings
^^^^^^^^^^^^^^

* Assuming using just virt-manager for your virtualization software.
    * Go to CPUs as we will need to do some manual adjustments here. Setting vCPUs doesn't translate well to Windows for some stupid reason.
    * Click the checkbox to manually set CPU topology
    * Settings Example:
        * Sockets 1
        * Cores 4
        * Threads 2
            * Think 1 CPU socket, with 4 physicals cores and setting 2 threads each. 
            * Reminder: Don't set threads to 8 thinking you're giving 8 treads here means 8 CPUs.
                * Ex. 1 socket, 4 cores, 8 threads means 4*8 = 32 threads. If you have a AMD 5950X, you just gave the VM everything.

* Other CPU considerations is pinning the CPU cores in the XML file if you have more than one CPU socket.

.. code-block::

    $ sudo numactl --hardware
    available: 1 nodes (0)
    node 0 cpus: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
    node 0 size: 32006 MB
    node 0 free: 2136 MB
    node distances:
    node   0 
    0:  10 


    # In the XML

    <vcpu placement='static'>8</vcpu> # Look for this line and for a 8 threaded VM place the following line. Keep the vCPUs given in numactl.
    <vcpu placement='static' cpuset='8,9,10,11,12,13,14,15,16'>8</vcpu> # Translated, this means I wan't cores 8-16 in use for the 8 I've assigned.


Video Settings
^^^^^^^^^^^^^^^^^

* First you will need to install the Qemu guest agent for windows.
    * Locate and install ``qemu-ga-x86_64.msi`` on the Windows VM.
        * Fedora has a good link for it, I would just google it up as the link can change whenever. This is simple to find.
    * Validate the agent is running in powershell
        * Be sure you open as Administrator!
        * ``C:\Users\Administrator> Get-Service QEMU-GA``

* In virt-manager, update settings for the VM.
    * In ``Display Spice``.
        * Type: Spice Server
        * Listen Type: None
        * OpenGL Checkbox checked and set to ``auto``.
    * In ``Video Virtio``.
        * Model: Virtio
        * 3D acceleration checkbox checked.

Disk Settings and Updating
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* I would make this at least a 60-80G disk

* If you need to expand the disk:
    * ``sudo qemu-img resize $PATH_TO_VM_IMAGE +10G``
    * OR
    * ``sudo virsh blockresize $VM_NAME $PATH_TO_VM_IMAGE 80G``

* Then startup the VM and you will need to use a 3rd party partition manager because Microsoft disk management tools have features that lacking.
    * AOMEI Parition Assistant - Download the free version
        * During install you will be prompted for a Demo or to right to the PRO version of the software.
        * Click ``skip`` otherwise you will install the Demo version which can't merge unallocated space with your install.
        * Launch the partition assitant and right click the unallocated space, select merge and follow the prompts.

Windows VM Extras - UEFI/Secureboot and TPM
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* If you installed with Legacy BIOS and want UEFI, proceed with these steps:
    * While the VM is up and your in the login screen, hold the ``shift`` key.
    * Press the GUI power button and then restart while continue holding the ``shift`` key and don't let go until you see a blue troubleshooting screen.
    * ``Troubleshooting`` -> ``Advanced options`` -> ``Login`` and then select ``command prompt``.
        * Run: ``mbr2gpt.exe /validate``
            * If at the end it says successful, then you can convert.
        * Run: ``mbr2gpt.exe /convert``
            * Ignore the ``Failed to update ReAgent.xml`` message
        * Shutdown the VM.
    * Update the XML File for the VM. Here is the section and be sure the paths are valid.
        * Note: The ``<os>`` Tags should already exist in the XML file, just add in what is missing in the block here.

.. code-block::

    <os>
    <type arch="x86_64" machine="pc-q35-7.0">hvm</type>
    <loader readonly="yes" type="pflash">/usr/share/OVMF/OVMF_CODE_4M.ms.fd</loader>
    <nvram>/var/lib/libvirt/qemu/nvram/win11_VARS.fd</nvram> <-- Don't worry if this exists as it will be generated automatically on next VM start.
    <bootmenu enable="no"/>
    </os>


* At this point the VM will be stuck in 1280 resolution.
* Fix by rebooting into the UEFI boot menu. The interrupt key is ``ESC``.
* Update video settings. The max I've seen is 1920x1080.

TPM Module Configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^

* These should be the defaults but just incase they're not...
    * Type: Emulated
    * Under Advanced Options
        * Model: CRB
        * Version: 2

Sound Settings
^^^^^^^^^^^^^^^^^^^

* Some funky work around with sound issues. Note that the ``AC97`` option failed to attach an output device for Windows 11.
    * Model: ``HDA(ICH6)``

Know better methods for Windows?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you have a better way, please see the home page of this documentation so you can see where you may inform me how to do this better. This whole process is silly!
